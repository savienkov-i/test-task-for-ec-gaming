﻿using UnityEngine;
using System.Collections;
using Zenject;

public class SpawnEnemyController : MonoBehaviour
{
	[Header("Settings Spawn")]
	[SerializeField] private float intervalSpawn;
    [SerializeField] private GameObject moveTargetEnemy;

    private EnemyController _enemyControllerTemp;
	private EnemyController _enemyController;
	private Coroutine spawnEnemyCoroutine;

    [Inject]
    private void Construct(EnemyController enemyController)
    {
        _enemyController = enemyController;
    }

    private void Start()
    {
		Initialization();
    }

	private void Initialization()
	{
		SpawnEnemy();
    }

	private void SpawnEnemy()
	{
        if (spawnEnemyCoroutine != null)
            StopCoroutine(spawnEnemyCoroutine);
        spawnEnemyCoroutine = StartCoroutine(SpawnEnemyCoroutine());
    }

	private IEnumerator SpawnEnemyCoroutine()
	{
		yield return new WaitForSeconds(intervalSpawn);

        _enemyControllerTemp = Instantiate(_enemyController, transform.position, Quaternion.identity);
        _enemyControllerTemp.SetTargetMove(moveTargetEnemy);
        SpawnEnemy();
    }
}