﻿using UnityEngine;
using Zenject;
using static UnityEngine.GraphicsBuffer;

public enum TypeCannonTower { Default, Mortar }

public class CannonTower : Tower 
{
    [Header("Settings Type Tower")]
    [SerializeField] private TypeCannonTower _typeCannonTower;

    [Header("Settings Rotation")]
    [SerializeField] private float speedRotation;
    [SerializeField] private GameObject objectToRotate;
    [SerializeField] private Vector3 offsetRotate;
    [SerializeField] private float angleInRadians;

    private CannonProjectile _cannonProjectile;
    private CannonProjectile _cannonProjectileTemp;
    private Quaternion targetRotation;

    [Inject]
    private void Construct(CannonProjectile cannonProjectile)
    {
        _cannonProjectile = cannonProjectile;
    }

    private Vector3 GetAimingDirection(float speedProjectile)
    {
        Vector3 direction = HitColliders[0].transform.position - transform.position;
        float timeToImpact = direction.magnitude / speedProjectile;
        Vector3 predictedPosition = HitColliders[0].transform.position + HitColliders[0].GetComponent<Rigidbody>().velocity * timeToImpact;
        Vector3 aimingDirection = predictedPosition - objectToRotate.transform.position + offsetRotate;

        return aimingDirection;
    }

    private float CalculateSpeedVelocity(Vector3 target)
    {
        float x = new Vector3(target.x, 0, target.z).magnitude;
        float y = target.y;

        float angle = angleInRadians * Mathf.PI / 180;

        float v2 = (Physics.gravity.y * x * x)  / (2 * (y - Mathf.Tan(angle) * x) * Mathf.Pow(Mathf.Cos(angle), 2));
        float v = Mathf.Sqrt(Mathf.Abs(v2));

        return v;
    }

    public override void Shot()
    {
        Rotate();
        base.Shot();
    }

    private void Rotate()
    {
        if (HitColliders.Length > 0)
            if (HitColliders[0] != null)
            {
                if (_typeCannonTower == TypeCannonTower.Default)
                {
                    targetRotation = Quaternion.LookRotation(GetAimingDirection(_cannonProjectile.Speed));
                    objectToRotate.transform.rotation = Quaternion.Slerp(objectToRotate.transform.rotation, targetRotation, speedRotation * Time.deltaTime);
                }
                else

                {
                    targetRotation = Quaternion.LookRotation(GetAimingDirection(CalculateSpeedVelocity(HitColliders[0].transform.position - ShootPoint.transform.position)));
                    objectToRotate.transform.rotation = Quaternion.Slerp(objectToRotate.transform.rotation, Quaternion.Euler(-angleInRadians, targetRotation.eulerAngles.y, 0), speedRotation * Time.deltaTime);
                }
            }
    }

    public override void SpawnProjectile()
    {
        _cannonProjectileTemp = Instantiate(_cannonProjectile, ShootPoint.position, ShootPoint.rotation);
        _cannonProjectileTemp.SetTypeProjectile(_typeCannonTower);
        if (_typeCannonTower == TypeCannonTower.Mortar)
            _cannonProjectileTemp.SetVelocity(ShootPoint, CalculateSpeedVelocity(GetAimingDirection(CalculateSpeedVelocity(HitColliders[0].transform.position - ShootPoint.transform.position))));
        base.SpawnProjectile();
    }
}