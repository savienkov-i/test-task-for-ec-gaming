using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour
{
    [Header("Settings Tower")]
    [SerializeField] private float shootInterval;
    [SerializeField] private float rangeToAttack;
    [SerializeField] private float rangeToDetect;
    [SerializeField] private Transform shootPoint;
    [SerializeField] private LayerMask layerMaskEnemy;

    private bool isCanShot = true;
    private Collider[] hitColliders;

    public Transform ShootPoint { get => shootPoint; }
    public Collider[] HitColliders { get => hitColliders; }

    private void Update()
    {
        CheckEnemy();
    }

    private void CheckEnemy()
    {
        hitColliders = Physics.OverlapSphere(transform.position, rangeToDetect, layerMaskEnemy);

        if (hitColliders.Length > 0)
            if (hitColliders[0] != null)
                Shot();
    }

    public virtual void Shot()
    {
        if (isCanShot == true && Vector3.Distance(transform.position, hitColliders[0].transform.position) < rangeToAttack)
        {
            isCanShot = false;

            SpawnProjectile();
            StartCoroutine(DelayShot());
        }
    }

    private IEnumerator DelayShot()
    {
        yield return new WaitForSeconds(shootInterval);
        isCanShot = true;
    }

    public virtual void SpawnProjectile() { }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, rangeToDetect);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, rangeToAttack);
    }
}