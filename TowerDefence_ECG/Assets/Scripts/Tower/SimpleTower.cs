﻿using UnityEngine;
using Zenject;

public class SimpleTower : Tower 
{
	private GuidedProjectile _guidedProjectile;
    private GuidedProjectile _guidedProjectileTemp;

    [Inject]
    private void Construct(GuidedProjectile guidedProjectile)
    {
        _guidedProjectile = guidedProjectile;
    }

    public override void SpawnProjectile()
    {
        _guidedProjectileTemp = Instantiate(_guidedProjectile, ShootPoint.position, Quaternion.identity);
        _guidedProjectileTemp.SetProjectile(HitColliders[0].gameObject);
        base.SpawnProjectile();
    }
}