using UnityEngine;

public abstract class IDamageable : MonoBehaviour
{
    public abstract void GetDamage(int damage);
}