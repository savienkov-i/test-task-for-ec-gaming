﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EnemyController : IDamageable 
{
	[Header("Settings Enenmy")]
    [SerializeField] private int healthPointMaximum;
	[SerializeField] private float speed;
    [SerializeField] private Rigidbody _rigidbody;

    private GameObject targetMove;
    private int healthPointCurrent;

	private void Start() 
	{
		Initialization();
    }

	private void Initialization()
	{
        healthPointCurrent = healthPointMaximum;
    }

    private void Update()
    {
        MoveEnemy();
    }

    private void MoveEnemy()
    {
        if (targetMove != null)
            _rigidbody.velocity = (targetMove.transform.position - transform.position).normalized * speed;
    }

    public void SetTargetMove(GameObject target)
    {
        targetMove = target;
    }

    public override void GetDamage(int damage)
    {
		healthPointCurrent -= damage;

		if (healthPointCurrent <= 0)
			Destroy(gameObject);
    }
}