﻿using UnityEngine;

public class GuidedProjectile : Projectile 
{
	private GameObject targetEnemy;

	private void Update ()
    {
        MoveProjectile();
    }

    private void MoveProjectile()
    {
        if (targetEnemy != null)
            transform.position = Vector3.MoveTowards(transform.position, targetEnemy.transform.position, Speed * Time.deltaTime);
        else
            Destroy(gameObject);
    }

    public void SetProjectile(GameObject enemy)
    {
        targetEnemy = enemy;
    }
}