﻿using System.Collections;
using UnityEngine;

public class CannonProjectile : Projectile 
{
    [Header("Settings Type Projectile")]
    [SerializeField] private TypeCannonTower _typeCannonProjectile;

    [Header("Settings Destroy Projectile")]
    [SerializeField] private int timeToDestroy;
    [SerializeField] private Rigidbody _rigidbody;

    private void Start()
    {
        Initialization();
    }

    private void Initialization()
    {
        if (_typeCannonProjectile == TypeCannonTower.Default)
            Destroy(_rigidbody);

        StartCoroutine(DestroyCoroutine());
    }

    private void Update () 
	{
        if (_typeCannonProjectile == TypeCannonTower.Default)
            MoveProjectile();
	}

    public void SetVelocity(Transform shootPoint, float v)
    {
        _rigidbody.velocity = shootPoint.forward * v;
    }
   
    public void SetTypeProjectile(TypeCannonTower typeCannonProjectile)
    {
        _typeCannonProjectile = typeCannonProjectile;
    }

    private void MoveProjectile()
    {
        transform.position += transform.forward * Speed * Time.deltaTime;
    }

    private IEnumerator DestroyCoroutine()
    {
        yield return new WaitForSeconds(timeToDestroy);
        Destroy(gameObject);
    }
}