using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Settings Projectile")]
    [SerializeField] private float speed;
    [SerializeField] private int damage;

    public float Speed { get => speed; }
    public int Damage { get => damage; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent(out IDamageable damageable))
        {
            damageable.GetDamage(Damage);
            Destroy(gameObject);
        }
    }
}