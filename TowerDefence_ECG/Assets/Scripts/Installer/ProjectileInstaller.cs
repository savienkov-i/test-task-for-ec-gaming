using UnityEngine;
using Zenject;

public class ProjectileInstaller : MonoInstaller
{
    [Header("Settings Projectile")]
    [SerializeField] private CannonProjectile _cannonProjectile;
    [SerializeField] private GuidedProjectile _guidedProjectile;

    public override void InstallBindings()
    {
        Container.Bind<CannonProjectile>().FromInstance(_cannonProjectile);
        Container.Bind<GuidedProjectile>().FromInstance(_guidedProjectile);
    }
}