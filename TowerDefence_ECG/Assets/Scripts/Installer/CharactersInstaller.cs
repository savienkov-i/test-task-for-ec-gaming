using UnityEngine;
using Zenject;

public class CharactersInstaller : MonoInstaller
{
    [Header("Settings Characters")]
    [SerializeField] private EnemyController _enemyController;

    public override void InstallBindings()
    {
        Container.Bind<EnemyController>().FromInstance(_enemyController);
    }
}